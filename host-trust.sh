for address in $@; do
  ssh-keygen -F $address 2>/dev/null 1>/dev/null
  if [ $? -eq 0 ]; then
    echo “$address is already known”
    continue
   fi
   echo "adding $address to known_hosts"
   ssh-keyscan -t rsa -T 10 $address >> ~/.ssh/known_hosts
done